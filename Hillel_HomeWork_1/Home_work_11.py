# 1. Доработайте классы Point и Line из занятия. 
# Обеспечьте передачу в first_point и second_point класса Line
# только обьектов класса Point с помощью property

from time import time

class Point:

    def __init__(self, coord_x, coord_y):
        if not isinstance(coord_x, (int, float)) and not isinstance (coord_y, (int, float)):
            raise TypeError
        self.__x = coord_x
        self.__y = coord_y

    x = property()
    y = property()

    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, value):
        self.__x = value

    @property
    def y(self):
        return self.__y

    @y.setter
    def y(self, value):
        self.__y = value

    def __str__(self):
        return f'Point with coords x = {self.__x}, y = {self.__y}'

class Line:

    def __init__(self, begin, end):
        self.__first_point = begin
        self.__second_point = end

    first_point = property()
    second_point = property()
    
    @property
    def first_point(self):
        return self.__first_point

    @first_point.setter
    def first_point(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self.__first_point = value

    @property
    def second_point(self):
        return self.__second_point

    @second_point.setter
    def second_point(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self.__second_point = value

    @property
    def length(self):
        x = (self.first_point.x - self.second_point.x) ** 2
        y = (self.first_point.y - self.second_point.y) ** 2
        return (x + y) ** 0.5

    def __str__(self):
        return f' Line from {self.first_point} to {self.second_point}'

line = Line(Point(0, 3), Point(4, 0))
print(line.length)


# 2. Напишите декоратор, который замеряет и принтует время выполнения функции


def fibonacci_func(arg):
            if arg in (1, 2):
                return 1
            
            fib1 = fib2 = 1

            for arg in range(arg - 2):
                fib1, fib2 = fib2, fib1 + fib2
            return fib2

def time_checker_func(func_to_decorate):

    def wrapper(*args, **kwargs):
        time_start = time()
        result = func_to_decorate(*args, **kwargs)
        print(f'Done in {str(time() - time_start)[:4]} seconds')
        return result
    
    return wrapper


fibonacci_func = time_checker_func(fibonacci_func)
print(fibonacci_func(150))

