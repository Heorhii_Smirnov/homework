# 1. Есть строка произвольного содержания.
#  Написать код, который найдет в строке самое короткое слово, 
#  в котором присутствуют подряд две гласные буквы.

vovels = 'eyuioa'
signs = '!?,.;:\'"'
words = input('Enter smth: ')
for sign in words:
    if sign in signs:
        words = words.replace(sign, '')
words = words.lower()
for vovel in words:
    if vovel in vovels:
        words = words.replace(vovel, '+')

list_with_spec_words = []
sign = '++'
words = words.split()
for word in words:
    if sign in word:
        list_with_spec_words.append(word)

for word in list_with_spec_words:
    list_with_spec_words.sort()
    shortest_word = list_with_spec_words[0]

print(shortest_word)

# если нет слов с двумя гласными и если несколько слов одинаковой длины 
# попробовать переписать без замены гласных на +



# for word in list_with_spec_words:
#     words_length = [len(word) for word in list_with_spec_words]
#     words_length.sort()
#     shortest_length = words_length[0]

#     for word in list_with_spec_words:
#         if len(word) == shortest_length:
#             shortest_word = word

# print(shortest_word)


# asoid, ggGFaakdf, asdAoDhf. asSFd!! aDFd? asoid, !!! ДЛЯ ПРОВЕРКИ ИСПОЛЬЗУЮ ЭТО !!!


# 2. Есть два числа - минимальная цена и максимальная цена.
#  Дан словарь продавцов и цен на какой то товар у разных продавцов:
#   { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324,
#    "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}.
#     Написать код, который найдет и выведет на экран список продавцов, чьи цены попадают в диапазон между нижней и верхней ценой. 
#     Например:
#     lower_limit = 35.9
# upper_limit = 37.3
# > match: "g-store", "royal-service"



my_dict = {
    'Citrus': 47.999,
    'Istudio': 42.999,
    'Moyo': 49.999,
    'Royal-service': 37.245,
    'Buy.ua': 38.324,
    'G-store': 37.166,
    'Ipartner': 38.988,
    'Sota': 37.720,
    'Rozetka': 38.003
    
}

result = [key for key,value in my_dict.items() if value > 35.9 and value < 37.3]

print(result)