# 1. Написать функцию, принимающую два аргумента. Функция должна :
# - если оба аргумента относятся к числовым типам - вернуть их произведение,
# - если к строкам - соединить в одну строку и вернуть,
# - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# в любом другом случае вернуть кортеж (tuple) из аргументов


def function(arg1, arg2):
    if type(arg1) is int and type(arg2) is int:
        result = arg1 * arg2
        return(result)
    elif type(arg1) is str and type(arg2) is str:
        result = arg1 + arg2
        return(result)
    elif type(arg1) is str and type(arg2) is int:
        dict = {arg1: arg2}
        return dict
    else:
        tuple = (arg1, arg2)
        return tuple

result = function(1, [])
print(result)


# 2. Пользователь вводит строку произвольной длины. Написать функцию, которая принимает строку и
# должна вернуть словарь следующего содержания:
# ключ - количество букв в слове, значение - list слов с таким количеством букв.
# отдельным ключем, например "0", записать количество пробелов.
# отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.
# Например:
# {
# "0": количество пробелов в строке
# "1": list слов из одной буквы
# "2": list слов из двух букв
# "3": list слов из трех букв
# и т.д ...
# "punctuation" : tuple уникальных знаков препинания
# }



def dict_funcion(message):
    text = input(f'{message}:\n')
    my_dict = {}
    my_dict.update({'0': 'Number of spaces - ' + str(text.count(' '))})
    signs = '.,!?;:'
    for sign in signs:
        text = text.replace(sign,'')
    words = text.split()
    for word in words:
        my_dict.update({len(word): [word]})
    if my_dict.keys == len(word):
        [word].append()
    return my_dict

result = dict_funcion('Enter something')
print(result)
