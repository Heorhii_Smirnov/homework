
# Напишіть функцію (хто може - клас) яка приймає довільний текст і знаходить в ньому всі номерні знаки України 
# і повертає їх у вигляді списку.

# Стандарти номерних знаків
# АB1234CD
# 12 345-67 AB
# a12345BC

import re

car_numbers = (
    r'[A-ZА-Я]{2}[0-9]{4}[A-ZА-Я]{2}',                 # 'АB1234CD'
    r'[0-9]{2}[ ]\d{3}\-\d{2}[ ][A-ZА-Я]{2}',         # '12 345-67 AB'
    r'[a-zа-я]{1}[0-9]{5}[A-ZА-Я]{2}',                 # 'a12345BC'
    )

def car_number_search_func(raw_text):
    car_numbers_list = []
    for number in car_numbers:
        car_numbers_list += re.findall(number, raw_text)

    print(car_numbers_list)

text = 'АВ1234CD, asd1234as, 12 345-67 АВ, da 123-32 as, 34 123-56 AD, g12345fg, a12345ВС'
car_number_search_func(text)



