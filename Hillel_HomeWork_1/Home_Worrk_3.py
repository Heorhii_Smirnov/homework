# Задание первое
# Сформировать строку, в которой содержится информация определенном слове в строке.
# Например "The [номер] symbol in [тут слово] is [значение символа по номеру в слове]". 
# Слово и номер получите с помощью input() или воспользуйтесь константой.
# Например (если слово - "Python" а символ 3) - "The 3 symbol in "Python" is t".



# while True:
#     try:   
#         number = int(input('Enter number: '))
#     except:
#         print('Something went wrong.')
#         continue
#     break
# while True:
#     if number <= 0:
#         print('Zero or Negative number')
#         number = int(input('Enter number: '))
#         continue
#     break
# while True:
#     word = input('Enter word: ')
#     if word.isalpha():
#         break
# try:
#     letter = word[number - 1]
#     sentence = f'The {number} symbol in {word} is {letter}'
#     print(sentence)
# except IndexError as letter:
#     print('Number is too big')



# Задание второе
# Ввести из консоли строку. 
# Определить количество слов в этой строке, которые заканчиваются на букву "o" 
# (учтите, что буквы бывают заглавными).


# sentence = input('Enter something:\n')
# lst = list(sentence.split(' '))
# counter = 0
# for element in lst:
#     if element.endswith('o') or element.endswith('O'):
#         counter += 1
# print(counter)


# Задание третье
# Есть list с данными lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишите механизм, который cформирует новый list (например lst2), 
# который содержит только переменные типа str, которые есть в lst1.



lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for element in lst1:
    if type(element) is str:
        lst2.append(element)
print(lst2)



