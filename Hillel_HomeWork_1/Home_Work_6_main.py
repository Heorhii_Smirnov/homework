# 1. В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N)
# и возвращает True/False в зависимости от того, что он ввел. 
# В основном файле (пусть будет main_file.py) попросить пользователя ввести с клавиатуры строку и вывести ее на экран. 
# Используя импортированную из lib.py функцию спросить у пользователя, хочет ли он повторить операцию (Y/N). 
# Повторять пока пользователь отвечает Y и прекратить когда пользователь скажет N.

from Home_Work_6_lib import repeat_func



while True:
    text = input('Please, enter something:\n')
    print(f'You wrote: {text}.')
    answer = repeat_func()
    if answer is False:
        break




# 2. Модифицируем ДЗ 2. Напишите с помощью функций!. 
# Помните о Single Responsibility! Попросить ввести свой возраст (можно использовать константу или input()). 
# Пользователь ввел значение возраста [year number] а на место [year string] нужно поставить правильный падеж существительного "год", 
# который зависит от значения [year number].
# если пользователь ввел непонятные данные (ничего не ввел, ввел не число, неактуальный возраст и тд.) - вывести “не понимаю”
# если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# если пользователю меньше 18 - вывести “Тебе [year number] [year string], а мы не продаем сигареты несовершеннолетним”
# если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# в любом другом случае - вывести “Оденьте маску, вам же [year number] [year string]!”
# Например:
# Тебе 1 год, где твои мама и папа?
# Оденьте маску, вам же 23 года!
# Вам уже 68 лет, вы в зоне риска!

# должно быть 3 функции
# 1 функция - анализ входных данных
# 2 функция - анализ года и вывод нужного варианта к слову "год" для числа 
# 3 функция - вывод соответсвующего сообщения 



# def user_data_input_analysis(*age_data):
#     while True:
#         user_age = input('Введите ваш возраст:\n')
#         if user_age.isdigit() and int(user_age) > 0:
#             break
#         else:
#             print('Я не понимаю, попробуйте еще раз')
#     return user_age   


# def year_data_analysis():
#     user_age = user_data_input_analysis() 
#     tpl = ('5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20')
#     for element in tpl:
#         if user_age == element in tpl:
#             year_data = f'лет'
#             return year_data
#     if user_age[-1] == '1':
#         year_data = f'год'
#         return year_data
#     elif user_age[-1] == '2' or user_age[-1] == '3' or user_age[-1] == '4':
#         year_data = f'года'
#         return year_data
#     else:
#             year_data = f'лет'
#     return year_data


# def message_to_age():
#     user_age = int(user_data_input_analysis())
#     year_data = year_data_analysis()
#     if user_age > 0 and user_age <= 7:
#         message = f'Тебе {user_age} {year_data}, где твои родители?'
#     elif user_age > 7 and user_age <= 18:
#         message = f'Тебе {user_age} {year_data}, мы не продаем сигареты несовершеннолетним'
#     elif user_age > 65:
#         message = f'Вам уже {user_age} {year_data}, вы в зоне риска!!!'
#     else:
#         message = f'Оденьте маску, вам же {user_age} {year_data}'
#     return message

# result = message_to_age()
# print(result)














