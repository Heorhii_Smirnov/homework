# 1. Пишем игру. Программа выбирает из диапазона чисел (пусть для начала будет 1-100) случайное число 
# и предлагает пользователю его угадать. Пользователь вводит число. 
# Если пользователь не угадал - предлагает пользователю угадать еще раз, пока он не угадает. 
# Если угадал - спрашивает хочет ли он повторить игру (Y/N). Если Y - повторить. N - Прекратить. 
# Опционально - добавьте в задание вывод сообщения-подсказки. 
# Если пользователь ввел число, и не угадал - сообщать: "Холодно" если разница между загаданным и введенным числами больше 10, 
# "Тепло" - если от 5 до 10 и "Горячо" если от 4 до 1.

# рандомное число от компа
# ввод пользователя с циклом, если не угадал
# сообщение подсказка
# повтор игры

# 23 4 

import random as rand

from Home_Work_6_lib import repeat_func

def random_computer_number_func(*integers):
    computer_number = (rand.randrange(1, 101))
    return computer_number


def try_2_guess_number_func(*integers):
    while True:
        user_number = input('Try to guess a random number in range from 1 to 100:\n')
        if user_number.isdigit() and int(user_number) in range(1, 101):
            break
        else:
            print('Not a number or not in range. Try one more time')
    return user_number


def guess_or_not_guess_func(user_number, computer_number):
    if user_number == computer_number:
        return True
    else:
        return False


def message_to_user(user_number, computer_number, game_winner):
    dct = {
        True: 'User',
        False: 'Computer' 
    }
    message = f'You choose {user_number}, computer choose {computer_number}. The winner is {dct[game_winner]}'

    return message


def rn_game():
    user_number = try_2_guess_number_func(rand.randrange(1, 101))

    computer_number = random_computer_number_func(rand.randrange(1, 101))

    game_winner = guess_or_not_guess_func(user_number, computer_number)

    message = message_to_user(user_number, computer_number, game_winner)

    return message


def one_more_time_func():
    while True:
        answer = repeat_func()
        if answer is False:
            break
        


    


result = rn_game()

print(result)





