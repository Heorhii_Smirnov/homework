# Доработайте игру с занятия следующим образом: добавьте новые игровые фигуры 
# (камень ножницы, бумага -> камень, ножницы, бумага, ящерица, Спок) 
# https://www.youtube.com/watch?v=A2BYHWFpgVQ . 
# Также игра должна записывать данные статистики в текстовый файл (пронумерованный список: дата и время, кто выиграл, что выбросили).
# Rock Scissors Paper Lizard Spock

from random import choice as random_choice, shuffle
from datetime import datetime

def user_choice(*variants):
    msg = f'Choose one of: {", ".join(variants)}: \n'
    while True:
        user_input = input(msg)
        if user_input not in variants:
            print('Wrong!')
        else:
            return user_input


def computer_choice(*variants):
    variants = list(variants)
    shuffle(variants)

    result = random_choice(variants)

    return result


def is_user_win(rules_to_win, user_figure, computer_figure):
    """
    Args:
        rules_to_win:
        user_figure:
        computer_figure:
    Returns:
        (bool|None)
    """
    if user_figure == computer_figure:
        return
    
    for element in rules_to_win[user_figure]:
        if computer_figure == element: 
            return True

    return False


def make_message(result, user_figure, computer_figure):

    dct = {
        True: 'User win',
        False: 'Computer win',
        None: 'Draw!',
    }

    msg = f'User - {user_figure}, computer - {computer_figure}, result is - {dct[result]}'

    return msg


def statistic_func(message):
    lines_counter = 1
    current_time = datetime.now()
    with open('statistic.txt', 'a+') as file:
            statistics = file.write(f'{lines_counter}: Time is {current_time}, {message}.\n')
    return statistics


    # file = open('statistic.txt', 'a+')
    # current_time = datetime.now()
    # lines_counter = 1
    # statistics = file.write(f'{lines_counter}: Time is {current_time}, {message}\n')
    # for lines_counter in file:
    #     lines_counter +=1
    # file.close()
    # return statistics

def rsp_game():
    figures = ('Rock', 'Scissors', 'Paper', 'Lizard', 'Spock')

    rules_to_win = {
        'Rock': ['Scissors', 'Lizard'],
        'Scissors': ['Paper', 'Lizard'],
        'Paper': ['Rock', 'Spock'],
        'Lizard': ['Spock', 'Paper'],
        'Spock': ['Scissors','Rock']
    }

    user_figure = user_choice(*rules_to_win.keys())

    computer_figure = computer_choice(*rules_to_win.keys())

    game_result = is_user_win(rules_to_win, user_figure, computer_figure)

    message = make_message(game_result, user_figure, computer_figure)

    statistics = statistic_func(message)

    return message


result = rsp_game()

print(result)
