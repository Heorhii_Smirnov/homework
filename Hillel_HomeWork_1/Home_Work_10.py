# Создайте класс "Транспортное средство" 
# и отнаследуйтесь от него классами "Самолет", "Автомобиль", "Корабль". 
# Продумайте и реализуйте в классе "Транспортное средство" общие атрибуты
# для "Самолет", "Автомобиль", "Корабль". 
# В наследниках реализуйте характерные для них атрибуты и методы


class Vehicle:
    # def __init__(self, control_system, engine, frame, parking_lights):
    #     self.control_system = control_system
    #     self.engine = engine
    #     self.frame = frame
    #     self.parking_lights = parking_lights

    # def info_func(self):
    #     return (self.control_system, self.engine, self.frame, self.parking_lights)

    control_system = 'steering wheel'
    engine = 'engine'
    frame = 'metal'
    parking_lights = 'parking lights'


class Car(Vehicle):
    fuel = ('gas', 'diesel', 'gasoline', 'electric')
    wheel = '4 wheels with tire'
    hood = 'hood'
    trunk = 'trunk'

    def car_func(self):
        car_msg = f'Car is a vehicle with {self.wheel}, {self.hood}, {self.trunk} and different types of fuel {self.fuel}'
        return car_msg

    def car_honk_func(self):
        honk = self.control_system  # func for making sound
        return honk

car = Car()

print(car.car_func())


class Airplane(Vehicle):
    fuel = 'diesel with sulfur addition'
    wheel = 'complex chassis system'
    wings = 'wings'
    stabilizer = 'stabilizer'
    flaps = 'flaps'

    def airplane_func(self):
        airplane_msg = f'Airplane is a vehicle with {self.wheel}, {self.wings}, {self.stabilizer}, {self.flaps} and fuel - {self.fuel}'
        return airplane_msg

    def airplane_up_down_func(self):
        up_down = self.flaps # func for going up or down
        return up_down

airplane = Airplane()

print(airplane.airplane_func())


class Ship(Vehicle):
    anchor = '2 anchors'
    sails = 'sails'
    mast = 'mast'
    keel = 'keel'
    propeller_screw = 'propeller screw'

    def ship_func(self):
        ship_msg = f'Ship is a vehicle with {self.anchor}, {self.sails}, {self.mast}, {self.keel} and {self.propeller_screw}'
        return ship_msg

    def ship_stop_func(self):
        ship_stop = self.anchor # func for stop a ship
        return ship_stop

ship = Ship()

print(ship.ship_func())



