# Подключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ), 
# отримайте теперішній курс валют и запишіть його в TXT-файл в такому форматі:
#  "[дата, на яку актуальний курс]"
# 1. [назва валюти 1] to UAH: [значення курсу валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу валюти 3]
# ...
# n. [назва валюти n] to UAH: [значення курсу валюти n]

# опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс


import requests
from datetime import date
from requests.exceptions import (JSONDecodeError, RequestException) 

def currency_UAH():
    current_date = date.today()
    try:
        some_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?.json'
        result = requests.get(some_url) 
        result.raise_for_status()
        with open ('currency.txt', 'wt') as file:
            for idx, item in enumerate(result.json()):
                file.write(f'{idx +1}: Date - {current_date}. {item["txt"] + " до UAH: "}{item["rate"]}\n')     
    except JSONDecodeError as e:
        print(f"Could not parse JSON: {str(e)}")
    except RequestException as e:
        print(f'Could not connect or get data: {str(e)}')
    
currency_UAH()


